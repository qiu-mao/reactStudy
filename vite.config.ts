import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";

export default defineConfig({
  plugins: [react()],
  base: './',
  server: {
    host: "0.0.0.0", // 默认生成所有的在线链接
    port: 3000, // 配置3000端口
    open: true, // 配置启动vite自动打开浏览器
    proxy: {
      "/api": {
        changeOrigin: true,
        rewrite: (api) => api.replace(/^\/api/, ""),
      },
    },
  },
});
