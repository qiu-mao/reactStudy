/** 导航类 */
export interface Itabbar {
  /** id */
  id: number;
  /** 导航名字 */
  name: string;
  /** 导航对于的图标 */
  icon: string;
  /** 路由地址 */
  pathname: string;
}
/** 推荐卡片 */
export interface ICard {
  /** 主键 */
  id: number,
  /** 用户头像 */
  userImg: string,
  /** 用户昵称 */
  userNick: string,
  /** 用户头衔 */
  userSku: string,
  /** 用户头衔的图标 */
  userSkuIcon: string,
  /** 文章题目 */
  paperTitle: string,
  /** 文章配图 */
  paperTitleImg: string,
  /** 文章头衔 */
  paperSku: string,
  /** 阅读量 */
  lookSum: number,
  /** 点赞数量 */
  likeSum: number,
  /** 收藏数量 */
  collectSum: number
}
/** 页码信息 */
export interface IPage {
  /** 页码 */
  pageIndex: number,
  /** 每页的大小 */
  pageSize: number
}