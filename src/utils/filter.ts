/**
 * 过滤时间，传入标准时间格式
 * @param time 传入标准时间格式
 * @returns
 */
const filterTime = (time: any) => {
  const date = new Date(time);
  const year = date.getFullYear().toString().padStart(4, "0");
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  const day = date.getDate().toString().padStart(2, "0");
  return `${year}年${month}月${day}日`;
};

export default { filterTime };
