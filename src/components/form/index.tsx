import React from 'react'
import './index.less'
/**
 * 传入的数据
 */
interface IProps {
    /**
     * 传入的表单的内容DOM
     */
    children: any,
    /**
     * 返回的提交事件
     * @param e 表单值
     * @returns
     */
    onSubmit: (e: any) => void
}
/**
 * 表单组件
 */
const form: React.FC<IProps> = ({ children, onSubmit }) => {
    const submit = (e: any) => {
        e.preventDefault()
        const formData = Object.fromEntries(new FormData(e.target))
        onSubmit(formData)
    }
    return (
        <form onSubmit={submit} className='form'>
            {children}
        </form>
    )
}

export default form