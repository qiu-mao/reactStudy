import React, { useState } from 'react'
import './index.less'
import { Dialog } from 'tdesign-mobile-react'

const promise: React.FC = () => {
    const [alertProps, setAlertProps] = useState<any>(true)
    return (
        <div className='promise'>
            <Dialog
                visible={alertProps}
                title='提示'
                content='请先登录'
                cancelBtn="取消"
                confirmBtn="确定"
                onClose={() => {
                    setAlertProps(false)
                }}
                onConfirm={() => {
                    setAlertProps(false)
                }}
            />
        </div>
    )
}

export default promise