import React from 'react';
import './index.less';
import { ICard } from '../../../utils/interface';

interface IProps {
  data: ICard;
}

const cardContent: React.FC<IProps> = ({ data }) => {
  const { paperTitle, lookSum, likeSum, collectSum, paperTitleImg } = data;
  return (
    <div className='card-content'>
      <div className='card-left'>
        <div className='card-paper-title'>{paperTitle}</div>
        <div className='card-footer'>{lookSum}阅读 · {likeSum}点赞 · {collectSum}收藏</div>
      </div>
      <img className='paper-title-img' src={paperTitleImg} alt="paperTitleImg" />
    </div>
  );
};

export default cardContent;
