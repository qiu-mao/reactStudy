import React from 'react';
import './index.less';
import { ICard } from '../../../utils/interface';

interface IProps {
  data: ICard;
}

const cardTitle: React.FC<IProps> = ({ data }) => {
  const { userImg, userNick, userSku, userSkuIcon } = data;
  return (
    <div className='card-title'>
      <img className='user-img' src={userImg} alt="userImg" />
      <div className='user-nick'>{userNick}</div>
      <div className='user-sku'>
        <i className={userSkuIcon}></i>
        <span className='ml2'>{userSku}</span>
      </div>
      <div className='card-more'>···</div>
    </div>
  );
};

export default cardTitle;
