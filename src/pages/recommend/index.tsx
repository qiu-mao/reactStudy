import React, { useEffect, useState } from 'react'
import './index.less'
import { ICard, IPage } from "../../utils/interface"
import CardTitle from './cardTtitle'
import CardContent from './cardContent'
import * as mock from "../../static/mock"
import Tabbar from '../app/tabbar'

const recommend: React.FC = () => {
    const [cardList, setCardList] = useState<ICard[]>([])
    const [pageInfor, setPageInfor] = useState<IPage>({
        pageIndex: 1,
        pageSize: 20
    })
    /**
     * 初始化数据
     */
    useEffect(() => {
        bottomLoading()
    }, [])
    /**
     * 监听触底加载
     */
    useEffect(() => {
        init()
    }, [pageInfor])
    /**
     * 初始化卡片数据
     */
    const init = async () => {
        const res = await mock.cardList() as ICard[]
        setCardList([...cardList, ...res])
    }
    const bottomLoading = () => {
        const el: any = document.querySelector('.loading')
        const observe = new IntersectionObserver(([{ isIntersecting }]) => {
            if (isIntersecting) {
                setPageInfor(pre => {
                    let { pageIndex } = pre
                    return { ...pre, pageIndex: ++pageIndex }
                })
            }
            // observe.disconnect() // 出现一次后取消监听
        })
        observe.observe(el)
    }
    return (
        <div className='recommend'>
            {cardList.map((item: ICard, index: number) => {
                return (
                    <div key={index} className='recommend-card'>
                        <CardTitle data={item} />
                        <CardContent data={item} />
                    </div>
                )
            })}
            <div className='loading'>加载中...</div>

            <Tabbar />
        </div>
    )
}

export default recommend
