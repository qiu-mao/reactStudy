import React, { useEffect, useState } from "react"
import "./index.less"
import { Itabbar } from '../../../utils/interface'
import { useNavigate } from 'react-router-dom'
import * as mock from '../../../static/mock'

const tabbar: React.FC = () => {
    const [tabbarList, setTabbarList] = useState<Itabbar[]>([])
    const [activeIndex, setActiveIndex] = useState<number>(0)
    const navigate = useNavigate()
    /**
     * 初始化底部导航
     */
    useEffect(() => {
        init()
    }, [])
    /**
     * 初始化底部导航
     */
    const init = () => {
        const tabbarList = mock.tabbarList()
        const activeIndex = tabbarList.findIndex(item => item.pathname === window.location.pathname)
        setTabbarList(tabbarList)
        setActiveIndex(activeIndex)
    };
    /**
     * 切换tab栏
     * @param item 点击的item
     * @param activeIndex 点击的下标
     */
    const changeTab = (item: Itabbar, activeIndex: number) => {
        const { pathname } = item
        setActiveIndex(activeIndex)
        navigate({ pathname })
    };
    return (
        <div className="tabbar-list">
            {tabbarList.map((item: Itabbar, index: number) => {
                return (
                    <div
                        key={item.id}
                        onClick={() => changeTab(item, index)}
                        className={`tabbar-item-default ${activeIndex === index && "tabbar-item-active"}`}>
                        <div className={item.icon}></div>
                        <div>{item.name}</div>
                    </div>
                )
            })}
        </div>
    )
}

export default tabbar
