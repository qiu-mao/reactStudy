import React from "react"
import { Routes, Route } from "react-router-dom"
import "./index.less"
import Tabbar from "./tabbar"
import Mine from "../mine"
import Index from "../index"
import Recommend from '../recommend'
import Classfiy from "../classfiy"
import Notice from "../notice"
import Header from "../header"
import Sticky from "../sticky"
import Grid from "../grid"
import Promise from "../promise"
import List from "../list"

const App: React.FC = () => {
    return (
        <div className="app">
            <div className="app-content">
                <Routes>
                    <Route path="/">
                        <Route path="/" element={[<Recommend />, <Tabbar />]}></Route>
                        <Route path="/mine" element={[<Mine />, <Tabbar />]}></Route>
                        <Route path="/index" element={[<Index />, <Tabbar />]}></Route>
                    </Route>
                    <Route path="/classfiy" element={<Classfiy />}></Route>
                    <Route path="/notice" element={<Notice />}></Route>
                    <Route path="/header" element={<Header />}></Route>
                    <Route path="/sticky" element={<Sticky />}></Route>
                    <Route path="/grid" element={<Grid />}></Route>
                    <Route path="/promise" element={<Promise />}></Route>
                    <Route path="/list" element={<List />}></Route>
                </Routes>
            </div>
        </div>
    )
}

export default App
