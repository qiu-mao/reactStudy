import React, { useState } from 'react'
import './index.less'

interface IProps { }

const grid: React.FC<IProps> = ({ }) => {
    const [list] = useState<number[]>([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20])
    return (
        <div className='grid-list'>
            {list.map((item: number, index: number) => {
                return (
                    <div
                        className='grid-item'
                        key={index}>
                        {item}
                    </div>
                )
            })}
        </div>
    )
}

export default grid