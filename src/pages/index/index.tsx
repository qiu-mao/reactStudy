import React, { useState } from "react"
import { useNavigate } from "react-router-dom"
import "./index.less"

const index: React.FC = () => {
    const navigate = useNavigate()
    const [nav] = useState<any>([
        { id: 1, name: "classfiy" },
        { id: 2, name: "grid" },
        { id: 3, name: "header" },
        { id: 4, name: "promise" },
        { id: 5, name: "list" },
        { id: 6, name: "sticky" },
        { id: 7, name: "notice" },
    ])
    const changePage = (name: string) => {
        navigate({
            pathname: `/${name}`
        })
    }
    return (
        <div className="index">
            {nav.map((item: any) => {
                return (
                    <div
                        onClick={() => changePage(item.name)}
                        className='nav_list'
                        key={item.id}>
                        {item.name}
                    </div>
                )
            })}
        </div>
    )
}

export default index
