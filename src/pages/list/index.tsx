import React, { useEffect, useState } from 'react'
import './index.less'

interface IProps { }

const list: React.FC<IProps> = ({ }) => {
    const [group, setGroup] = useState<any>([])
    const [domList, setDomList] = useState<any>([])
    const [pageSize] = useState<number>(8)
    useEffect(() => {
        const group = []
        for (let i = 0; i < 210; i++) {
            group.push({
                id: i,
                name: `name${i}`,
                age: i,
                sex: i % 2 === 0 ? '男' : '女',
                address: `address${i}`,
                phone: `138${i}`,
                email: `email${i}`,
                createTime: new Date(),
                updateTime: new Date(),
                status: i % 2 === 0 ? '启用' : '禁用'
            })
        }
        setGroup(group)
        setDomList(group.slice(0, pageSize))
    }, [])
    const onscroll = (e: any) => {
        const { target: { scrollTop } } = e
        const startIndex = Math.ceil(scrollTop / 50)
        const endIndex = startIndex + pageSize
        setDomList(group.slice(startIndex, endIndex))
    }
    return (
        <div className='group-list' style={{ height: `${pageSize * 50}px` }} onScroll={(e) => onscroll(e)}>
            <div className='group-back' style={{ height: `${50 * group.length}px` }}>
                <div className='dom-list'>
                    {domList.map((item: any, index: number) => {
                        return (
                            <div key={index} className='group-item-out'>
                                <div className='group-item-inner'>
                                    <span>{item.id}</span>
                                    <span>{item.name}</span>
                                    <span>{item.age}</span>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}

export default list