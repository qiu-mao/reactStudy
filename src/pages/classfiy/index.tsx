import React, { useState } from 'react'
import './index.less'

interface IProps { }

const classfiy: React.FC<IProps> = ({ }) => {
    const [list] = useState<number[]>([1, 2, 3, 4, 5, 6, 7, 8, 9])
    return (
        <div className='list'>
            {list.map((item: number, index: number) => {
                return (
                    <div className='item' key={index}>{item}</div>
                )
            })}
        </div>
    )
}

export default classfiy