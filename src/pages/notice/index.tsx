import React, { useState } from 'react'
import './index.less'

interface IProps { }

const notice: React.FC<IProps> = ({ }) => {
    const [list] = useState<number[]>([1, 2, 3])
    return (
        <div className='notice'>
            <div className='notice-list'>
                {list.map((item: number, index: number) => {
                    return (
                        <div className='notice-item' key={index}>{item}</div>
                    )
                })}
            </div>
            <div className='notice-logo'>LOGO</div>
        </div>
    )
}

export default notice