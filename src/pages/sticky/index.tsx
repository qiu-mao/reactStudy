import React, { useState } from 'react'
import './index.less'

interface IProps { }

const sticky: React.FC<IProps> = ({ }) => {
    const [list] = useState<number[]>([1, 2, 3, 4, 5, 6, 7, 8, 9])
    return (
        <div className='sticky-list'>
            {list.map((item: number, index: number) => {
                return (
                    <div className='sticky-item' key={index}>{item}</div>
                )
            })}
        </div>
    )
}

export default sticky