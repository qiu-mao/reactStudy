import React, { useState } from 'react'
import './index.less'

interface IProps { }

const header: React.FC<IProps> = ({ }) => {
    const [list] = useState<string[]>(['首页', '分类页', '管理页', '个人信息', '首页', '分类页', '管理页', '个人信息'])
    return (
        <div className='header-top'>
            {list.map((item: string, index: number) => {
                return (
                    <div className={`${index === list.length - 1 && 'header-top-last'} header-top-item`} key={index}>{item}</div>
                )
            })}
        </div>
    )
}

export default header