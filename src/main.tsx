import ReactDOM from "react-dom/client"
import App from "./pages/app"
import "./utils/index.less"
import "./static/icon.less"
import { BrowserRouter } from "react-router-dom"
import 'tdesign-mobile-react/es/style/index.css'

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
)
